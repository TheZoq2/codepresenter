use std::collections::BTreeMap;

use crate::ast::Ast;
use indoc::formatdoc;
use itertools::Itertools;

pub trait Backend {
    fn emit(&self, ast: &Ast, cfgs: &BTreeMap<String, String>) -> String;
}

pub struct Latex {}

impl Latex {
    fn build_slide(&self, ast: &Ast, i: usize) -> String {
        match ast {
            Ast::Cfg(_) => "".to_string(),
            Ast::Seq(inner) => inner.iter().map(|a| self.build_slide(a, i)).join(""),
            Ast::Show(seq) => seq
                .iter()
                .filter_map(|(ts, v)| ts.contains(i).then(|| self.build_slide(v, i)))
                .join(""),
            Ast::Text(t) => t.clone(),
            Ast::Highlight(duration, content) => {
                let inner = self.build_slide(content, i);

                if duration.contains(i) {
                    format!("@h{inner}h@")
                } else {
                    inner
                }
            }
        }
    }
}

impl Backend for Latex {
    fn emit(&self, ast: &Ast, cfgs: &BTreeMap<String, String>) -> String {
        let slide_count = ast.slide_count() as usize;

        let lang_attr = cfgs.get("lang").map(|l| format!("language={l}"));
        let lst_extra = cfgs.get("lst_extra").map(|l| l.to_string());
        let attrs = vec![lang_attr, lst_extra]
            .into_iter()
            .filter_map(|x| x)
            .join(", ");

        let content = (1..slide_count + 1)
            .map(|i| {
                let slide = self.build_slide(ast, i);
                formatdoc!(
                    r#"
                \begin{{onlyenv}}<{i}>
                    \begin{{lstlisting}}[{attrs}]
                {slide}
                    \end{{lstlisting}}
                \end{{onlyenv}}"#
                )
            })
            .join("\n");

        content
    }
}

pub struct Typst {
    pub prelude: String,
}

impl Typst {
    fn build_slide(&self, ast: &Ast, i: usize, lang: &str, highlight: bool) -> String {
        match ast {
            Ast::Cfg(_) => "".to_string(),
            Ast::Seq(inner) => inner
                .iter()
                .map(|a| self.build_slide(a, i, lang, highlight))
                .join(""),
            Ast::Show(seq) => seq
                .iter()
                .filter_map(|(ts, v)| {
                    ts.contains(i)
                        .then(|| self.build_slide(v, i, lang, highlight))
                })
                .join(""),
            Ast::Text(t) => {
                let result = t
                    .lines()
                    .into_iter()
                    .map(|line| {
                        let inner = line.replace("\\", "\\\\").replace("\"", "\\\"");

                        // Horrible workaround for https://github.com/typst/typst/issues/597
                        if line.chars().all(|c| c == ' ') {
                            let dashes = line.chars().map(|_| '-').collect::<String>();
                            // If typst doesn't render our whitespace, we'll just give it invisible
                            // characters :)
                            format!(r#"#box(text(raw("{dashes}"), fill: rgb(0, 0, 0, 0)))"#)
                        } else {
                            if highlight {
                                format!(r#"#box(raw("{inner}", lang: "{lang}"))"#)
                            } else {
                                format!(r#"#box(text(raw("{inner}"), fill: unfocused_color))"#)
                            }
                        }
                    })
                    .join("\\\n");

                if t.ends_with("\n") {
                    result + "\\\n"
                } else {
                    result
                }
            }
            Ast::Highlight(duration, content) => {
                if duration.contains(i) {
                    self.build_slide(content, i, lang, true)
                } else {
                    self.build_slide(content, i, lang, highlight)
                }
            }
        }
    }
}

impl Backend for Typst {
    fn emit(&self, ast: &Ast, cfgs: &BTreeMap<String, String>) -> String {
        let slide_count = ast.slide_count() as usize;

        let lang_attr = cfgs.get("lang").cloned().unwrap_or_default();

        let content = (1..slide_count + 1)
            .map(|i| {
                let slide = self.build_slide(ast, i, &lang_attr, false);
                formatdoc!(
                    r#"
                    #only({i}, [{slide}])
                    "#,
                )
            })
            .join("\n");

        format!("{prelude}\n{content}", prelude = self.prelude)
    }
}

#[cfg(test)]
mod latex_test {
    use super::Backend;
    macro_rules! snapshot_output {
        ($name:ident, $code:expr) => {
            #[test]
            fn $name() {
                let ast = crate::testutil::parse_code(&$code);

                let cfgs = ast.cfgs().unwrap();

                insta::assert_snapshot!(super::Latex {}.emit(&ast, &cfgs))
            }
        };
    }

    snapshot_output! {
        basic_hl_works,
        "@h1-2[fn@] main"
    }

    snapshot_output! {
        multi_hl_works,
        "@h1[fn@] @h2[main@]"
    }

    snapshot_output! {
        lang_attribute_works,
        "@@lang=spade
        fn main()"
    }

    snapshot_output! {
        lst_extra_works,
        "@@lang=spade
        @@lst_extra=style=base
        fn main()"
    }

    snapshot_output! {
        show_works,
        "@s1-1[fn@]@3-3[pipeline@]"
    }

    snapshot_output! {
        show_with_shorthand_works,
        "@s1[fn@]@3[pipeline@]"
    }

    snapshot_output! {
        escaping_at_works,
        r#"\@cocotb"#
    }

    snapshot_output! {
        cocotb_escaping_works,
        r#"
        # top=peripherals::timer::timer_test_harness

        # imports

        \@cocotb.test()
        async def timer_works(dut):"#
    }

    snapshot_output! {
        backslash_is_included,
        r#"\test"#
    }
}

#[cfg(test)]
mod typst_test {
    use indoc::formatdoc;

    use super::Backend;
    macro_rules! snapshot_output {
        ($name:ident, $code:expr) => {
            #[test]
            fn $name() {
                let ast = crate::testutil::parse_code(&$code);

                let cfgs = ast.cfgs().unwrap();

                insta::assert_snapshot!(super::Typst {
                    prelude: "prelude\n".to_string()
                }
                .emit(&ast, &cfgs))
            }
        };
    }

    snapshot_output! {
        newlines_are_handled_correctly,
        formatdoc!(r#"
            @h1[line 1@]
            @h2[line 2@]
            @h3[line 3@]
        "#)
    }

    snapshot_output! {
        recursive_highlights_are_highlighted,
        formatdoc!(r#"
            @h1[outer @h2[ inner@]@]
        "#)
    }

    snapshot_output! {
        indented_block_renders_correctly,
        formatdoc!(r#"
            level1
                @h1[level 2@]
        "#)
    }
}

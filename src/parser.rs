use colored::Colorize;
use spade_common::location_info::{lspan, AsLabel, FullSpan, HasCodespan, Loc, WithLocation};
use spade_diagnostics::{diag_bail, Diagnostic};
use spade_macros::trace_parser;
use tracing::{event, Level};

use logos::Lexer;

use crate::{
    ast::{Ast, Timespan},
    lexer::{CommandToken, DualLexer, LexerKind, TextToken, TokenKind},
};

type Result<T> = std::result::Result<T, spade_diagnostics::Diagnostic>;

/// A token with location info
#[derive(Clone, Debug, PartialEq)]
pub struct Token {
    pub kind: TokenKind,
    pub span: logos::Span,
    pub file_id: usize,
}

impl Token {
    pub fn new(kind: TokenKind, lexer: &DualLexer, file_id: usize) -> Self {
        Self {
            kind,
            span: lexer.span(),
            file_id,
        }
    }

    pub fn loc(&self) -> Loc<()> {
        Loc::new((), self.span.codespan(), self.file_id)
    }
}

impl HasCodespan for Token {
    fn codespan(&self) -> codespan::Span {
        self.span().codespan()
    }
}

impl AsLabel for Token {
    fn file_id(&self) -> usize {
        self.file_id
    }

    fn span(&self) -> std::ops::Range<usize> {
        self.span.clone()
    }
}

impl From<Token> for FullSpan {
    fn from(token: Token) -> FullSpan {
        (token.codespan(), token.file_id)
    }
}

// Clone for when you want to call a parse function but maybe discard the new parser state
// depending on some later condition.
#[derive(Clone)]
pub struct Parser<'a> {
    lex: DualLexer<'a>,
    peeked: Option<Token>,
    // The last token that was eaten. Used in eof diagnostics
    last_token: Option<Token>,
    pub parse_stack: Vec<ParseStackEntry>,
    file_id: usize,
}

impl<'a> Parser<'a> {
    pub fn new(lex: Lexer<'a, TextToken>, file_id: usize) -> Self {
        Self {
            lex: DualLexer {
                kind: Some(LexerKind::Text(lex)),
            },
            peeked: None,
            last_token: None,
            parse_stack: vec![],
            file_id,
        }
    }
}

/// Peek the next token. If it matches the specified token, get that token
/// otherwise return Ok(none)
// macro_rules! peek_for {
//     ($self:expr, $token:expr) => {
//         if let Some(t) = $self.peek_and_eat($token)? {
//             t
//         } else {
//             return Ok(None);
//         }
//     };
// }

// Actual parsing functions
impl<'a> Parser<'a> {
    #[trace_parser]
    pub fn top_level(&mut self) -> Result<Ast> {
        let result = self.block();

        let next = self.peek()?;
        if !matches!(next.kind, TokenKind::Eof) {
            Err(
                Diagnostic::error(next, "Extra command tokens at the end of the file")
                    .primary_label("Expected text or end of file"),
            )
        } else {
            result
        }
    }

    #[trace_parser]
    fn block(&mut self) -> Result<Ast> {
        let mut result = vec![];
        loop {
            // If we encounter a block end, we want to bail early, to let the
            // outer parser handle that
            if self.peek_kind(&TokenKind::CloseBlock)? == true {
                break;
            }

            let next = self.eat_unconditional()?;
            match next.kind {
                TokenKind::Eof => break,
                TokenKind::CloseBlock => unreachable!(),
                TokenKind::Text(t) => result.push(Ast::Text(t)),
                TokenKind::At => {
                    // the parser is now in command mode, parse a command
                    result.push(self.command()?)
                }
                TokenKind::Command(_) => {
                    diag_bail!(next, "Command outside command mode")
                }
                TokenKind::Error => diag_bail!(next, "Lexer error"),
            }
        }

        Ok(Ast::Seq(result))
    }

    fn in_block(&mut self) -> Result<Ast> {
        let start = self.eat(&TokenKind::Command(CommandToken::OpenBlock))?;

        // This marks the end of the command, exit command mode and parse top level
        // until we hopefully find a block
        self.command_done();

        let inner = self.block()?;

        let next = self.eat_unconditional()?;
        match &next.kind {
            TokenKind::Eof => Err(Diagnostic::error(next, "Unmatched block")
                .primary_label("Expected @]")
                .secondary_label(start, "To close this")),
            TokenKind::CloseBlock => Ok(inner),
            _ => diag_bail!(next, "Expected end of file or @["),
        }
    }

    #[trace_parser]
    fn command(&mut self) -> Result<Ast> {
        let next = self.eat_unconditional()?;

        match &next.kind {
            TokenKind::Command(cmd) => match cmd {
                CommandToken::Highlight => {
                    if let Some(span) = self.timespan()? {
                        let inner = self.in_block()?;

                        Ok(Ast::Highlight(span, Box::new(inner)))
                    } else {
                        Err(Diagnostic::error(next, "Expected timespan")
                            .primary_label("Expected timespan for this highlight"))
                    }
                }
                CommandToken::Show => {
                    let mut result = vec![];
                    loop {
                        if let Some(ts) = self.timespan()? {
                            let inner = self.in_block()?;
                            result.push((ts, inner));

                            // peek for the at to re-enter command mode for another
                            // specification. If it is not present, we are done
                            // parsing the show block
                            if self.peek_and_eat(&TokenKind::At)?.is_none() {
                                break;
                            }
                        }
                    }

                    Ok(Ast::Show(result))
                }
                CommandToken::ConfigLine((key, val)) => {
                    self.command_done();
                    Ok(Ast::Cfg(Loc::new(
                        (key.clone(), val.clone()),
                        next.codespan(),
                        self.file_id,
                    )))
                }
                _ => {
                    return Err(Diagnostic::error(
                        next.clone(),
                        format!(
                            "Expected command start (r, h etc.), got {}",
                            next.kind.as_str()
                        ),
                    )
                    .primary_label("Expected start of command"))
                }
            },
            TokenKind::Eof => {
                return Err(Diagnostic::error(
                    next,
                    "Got end of file when looking for command",
                ))
            }
            _ => diag_bail!(next, "Got non command in command mode"),
        }
    }

    fn timespan(&mut self) -> Result<Option<Timespan>> {
        let next = self.peek()?;

        let start = match &next.kind {
            TokenKind::Command(CommandToken::Integer(val)) => {
                self.eat_unconditional()?;
                Some(val.clone())
            }
            _ => None,
        };

        let (dash, end) = if let TokenKind::Command(CommandToken::Dash) = self.peek()?.kind {
            let dash = self.eat_unconditional()?;

            let next = self.peek()?;
            match &next.kind {
                TokenKind::Command(CommandToken::Integer(val)) => {
                    self.eat_unconditional()?;
                    (Some(dash), Some(val.clone()))
                }
                _ => (Some(dash), None),
            }
        } else {
            // If there is no dash, but there was a start, we want end to be start
            (None, start)
        };

        if start.is_some() || dash.is_some() || end.is_some() {
            Ok(Some(Timespan { start, end }))
        } else {
            Ok(None)
        }
    }
}

// Helper functions for combining parsers
impl<'a> Parser<'a> {
    #[tracing::instrument(skip_all, fields(parsers = parsers.len()))]
    fn first_successful<T>(
        &mut self,
        parsers: Vec<&dyn Fn(&mut Self) -> Result<Option<T>>>,
    ) -> Result<Option<T>> {
        for parser in parsers {
            match parser(self) {
                Ok(Some(val)) => {
                    event!(Level::INFO, "Parser matched");
                    return Ok(Some(val));
                }
                Ok(None) => continue,
                Err(e) => return Err(e),
            }
        }
        event!(Level::INFO, "No parser matched");
        Ok(None)
    }

    /// Attempts to parse an inner structure surrounded by two tokens, like `( x )`.
    ///
    /// If the `start` token is not found, an error is produced.
    ///
    /// If the `start` token is found, but the inner parser returns `None`, `None` is returned.
    ///
    /// If the end token is not found, return a mismatch error
    #[tracing::instrument(level = "debug", skip(self, inner))]
    fn surrounded<T>(
        &mut self,
        start: &TokenKind,
        inner: impl Fn(&mut Self) -> Result<T>,
        end_kind: &TokenKind,
    ) -> Result<(T, Loc<()>)> {
        let opener = self.eat(start)?;
        let result = inner(self)?;
        // FIXME: Better error handling here. We are throwing away potential EOFs
        let end = if let Some(end) = self.peek_and_eat(end_kind)? {
            end
        } else {
            return Err(Diagnostic::error(
                self.eat_unconditional()?,
                format!("Expected {}", end_kind.as_str()),
            )
            .secondary_label(opener, "To close this"));
        };

        Ok((
            result,
            Loc::new((), lspan(opener.span).merge(lspan(end.span)), self.file_id),
        ))
    }
}

// Helper functions for advancing the token stream
impl<'a> Parser<'a> {
    fn command_done(&mut self) {
        self.parse_stack.push(ParseStackEntry::ExitCommandMode);
        self.lex.command_done();
    }
    fn eat(&mut self, expected: &TokenKind) -> Result<Token> {
        self.parse_stack
            .push(ParseStackEntry::EatingExpected(expected.clone()));
        // Calling keep and eat in order to correctly handle >> as > > if desired
        let next = self.eat_unconditional()?;
        if &next.kind == expected {
            Ok(next)
        } else {
            Err(
                Diagnostic::error(next.clone(), format!("Unexpected `{}`", next.kind.as_str()))
                    .primary_label(format!("Expected {}", expected.as_str())),
            )
        }
    }

    #[allow(dead_code)]
    fn eat_cond(
        &mut self,
        condition: impl Fn(&TokenKind) -> bool,
        expected_description: &'static str,
    ) -> Result<Token> {
        // Check if we already have a peeked token
        let next = self.eat_unconditional()?;

        // Make sure we ate the correct token
        if !condition(&next.kind) {
            Err(
                Diagnostic::error(next.clone(), format!("Unexpected `{}`", next.kind.as_str()))
                    .primary_label(format!("Expected {}", expected_description)),
            )
        } else {
            Ok(next)
        }
    }

    fn eat_unconditional(&mut self) -> Result<Token> {
        let food = self
            .peeked
            .take()
            .map(Ok)
            .unwrap_or_else(|| self.next_token())?;

        self.parse_stack.push(ParseStackEntry::Ate(food.clone()));
        self.last_token = Some(food.clone());
        Ok(food)
    }

    /// Peeks the next token. If it is the specified kind, returns that token, otherwise
    /// returns None.
    ///
    /// If kind is > and the peeking is also done for >>, which if found, is split
    /// into > which is returned, and > which populates the peek buffer
    #[allow(dead_code)]
    fn peek_and_eat(&mut self, kind: &TokenKind) -> Result<Option<Token>> {
        // peek_cond_no_tracing because peek_kind handles >> -> > > transformation
        // which we don't want here
        if self.peek_kind(kind)? {
            Ok(Some(self.eat(kind)?))
        } else {
            Ok(None)
        }
    }

    fn peek(&mut self) -> Result<Token> {
        if let Some(peeked) = self.peeked.clone() {
            Ok(peeked)
        } else {
            let result = match self.next_token() {
                Ok(token) => token,
                Err(e) => return Err(e),
            };
            self.peeked = Some(result.clone());
            Ok(result)
        }
    }

    fn peek_kind(&mut self, expected: &TokenKind) -> Result<bool> {
        let result = self.peek_cond_no_tracing(|kind| kind == expected)?;
        self.parse_stack
            .push(ParseStackEntry::PeekingFor(expected.clone(), result));
        Ok(result)
    }

    /// Peek the next token, returning true if the result satisfies the condition.
    ///
    /// If we reached EOF and the peek returns None, returns false
    #[allow(dead_code)]
    fn peek_cond(&mut self, cond: impl Fn(&TokenKind) -> bool, msg: &str) -> Result<bool> {
        let result = self.peek_cond_no_tracing(cond)?;
        self.parse_stack.push(ParseStackEntry::PeekingWithCondition(
            msg.to_string(),
            result,
        ));
        Ok(result)
    }

    fn peek_cond_no_tracing(&mut self, cond: impl Fn(&TokenKind) -> bool) -> Result<bool> {
        self.peek().map(|token| cond(&token.kind))
    }

    fn next_token(&mut self) -> Result<Token> {
        let tok = self
            .lex
            .next()
            .map(|k| Token::new(k, &self.lex, self.file_id))
            .unwrap_or({
                match &self.last_token {
                    Some(last) => Token {
                        kind: TokenKind::Eof,
                        span: last.span.end..last.span.end,
                        file_id: last.file_id,
                    },
                    None => Token {
                        kind: TokenKind::Eof,
                        span: logos::Span { start: 0, end: 0 },
                        file_id: self.file_id,
                    },
                }
            });

        if let TokenKind::Error = tok.kind {
            return Err(Diagnostic::error(
                ().at(self.file_id, &self.lex.span()),
                "Lexer error",
            ));
        };

        Ok(tok)
    }
}

#[derive(Clone)]
pub enum ParseStackEntry {
    Enter(String),
    Ate(Token),
    #[allow(dead_code)]
    PeekingWithCondition(String, bool),
    PeekingFor(TokenKind, bool),
    EatingExpected(TokenKind),
    ExitCommandMode,
    Exit,
    ExitWithError(Diagnostic),
}
pub fn format_parse_stack(stack: &[ParseStackEntry]) -> String {
    let mut result = String::new();
    let mut indent_amount = 0;

    for entry in stack {
        let mut next_indent_amount = indent_amount;
        let message = match entry {
            ParseStackEntry::Enter(function) => {
                next_indent_amount += 1;
                format!("{} `{}`", "trying".white(), function.blue())
            }
            ParseStackEntry::Ate(token) => format!(
                "{} '{}'",
                "Eating".bright_yellow(),
                token.kind.as_str().bright_purple()
            ),
            ParseStackEntry::PeekingFor(kind, success) => format!(
                "{} {} {}",
                "peeking for".white(),
                kind.as_str().bright_blue(),
                if *success {
                    "✓".green()
                } else {
                    "𐄂".red()
                }
            ),
            ParseStackEntry::PeekingWithCondition(needle, success) => format!(
                "{} {} {}",
                "peeking conditionally for ".white(),
                needle.bright_blue(),
                if *success {
                    "✓".green()
                } else {
                    "𐄂".red()
                }
            ),
            ParseStackEntry::EatingExpected(kind) => {
                format!(
                    "{} {}",
                    "eating expected".purple(),
                    kind.as_str().bright_purple()
                )
            }
            ParseStackEntry::ExitCommandMode => {
                format!("{}", "exiting command mode".green())
            }
            ParseStackEntry::Exit => {
                next_indent_amount -= 1;
                format!("")
            }
            ParseStackEntry::ExitWithError(err) => {
                next_indent_amount -= 1;
                format!("{} {:?}", "Giving up: ".bright_red(), err)
            }
        };
        if let ParseStackEntry::Exit = entry {
        } else {
            for _ in 0..indent_amount {
                result += "| ";
            }
            result += &message;
            result += "\n"
        }
        indent_amount = next_indent_amount;
    }
    result
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::ast::Ast;

    use logos::Logos;

    #[macro_export]
    macro_rules! check_parse {
        ($string:expr , $method:ident$(($($arg:expr),*))?, $expected:expr$(, $run_on_parser:expr)?) => {
            let mut parser = Parser::new(TextToken::lexer($string), 0);

            $($run_on_parser(&mut parser);)?

            let result = parser.$method($($($arg),*)?);
            // This is needed because type inference fails for some unexpected reason
            let expected: Result<_> = $expected;

            if result != expected {
                println!("Parser state:\n{}", format_parse_stack(&parser.parse_stack));
                panic!(
                    "\n\n     {}: {:?}\n{}: {:?}",
                    "Got".red(),
                    result,
                    "Expected".green(),
                    expected
                );
            };
        };
    }

    #[test]
    fn start_only_timestamp_works() {
        let code = "1";

        let expected = Timespan {
            start: Some(1),
            end: Some(1),
        };

        check_parse!(code, timespan, Ok(Some(expected)), |p: &mut Parser| p
            .lex
            .enter_command_mode());
    }

    #[test]
    fn end_only_timestamp_works() {
        let code = "-1";

        let expected = Timespan {
            start: None,
            end: Some(1),
        };

        check_parse!(code, timespan, Ok(Some(expected)), |p: &mut Parser| p
            .lex
            .enter_command_mode());
    }

    #[test]
    fn dual_timestamp_works() {
        let code = "1-2";

        let expected = Timespan {
            start: Some(1),
            end: Some(2),
        };

        check_parse!(code, timespan, Ok(Some(expected)), |p: &mut Parser| p
            .lex
            .enter_command_mode());
    }

    #[test]
    fn start_continue_timestamp_works() {
        let code = "1-";
        let expected = Timespan {
            start: Some(1),
            end: None,
        };

        check_parse!(code, timespan, Ok(Some(expected)), |p: &mut Parser| p
            .lex
            .enter_command_mode());
    }

    #[test]
    fn highlight_one_frame_works() {
        let code = "outer @h1[inner@] outer";

        let expected = Ast::Seq(vec![
            Ast::text("outer "),
            Ast::hl(Some(1), Some(1), vec![Ast::text("inner")]),
            Ast::text(" outer"),
        ]);

        check_parse!(code, top_level, Ok(expected));
    }

    #[test]
    fn nested_highlight_one_frame_works() {
        let code = "outer @h1[inner@h2[inner2@]@] outer";

        let expected = Ast::Seq(vec![
            Ast::text("outer "),
            Ast::hl(
                Some(1),
                Some(1),
                vec![
                    Ast::text("inner"),
                    Ast::hl(Some(2), Some(2), vec![Ast::text("inner2")]),
                ],
            ),
            Ast::text(" outer"),
        ]);

        check_parse!(code, top_level, Ok(expected));
    }

    #[test]
    fn escape_at_works() {
        let code = r#"\@test"#;

        let expected = Ast::Seq(vec![Ast::text("@"), Ast::text("test")]);

        check_parse!(code, top_level, Ok(expected));
    }
}

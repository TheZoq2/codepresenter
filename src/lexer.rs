use std::ops::Range;

use logos::{Lexer, Logos};

#[derive(Logos, Debug, PartialEq, Clone)]
pub enum TextToken {
    #[regex(r#"([^@\\]*)"#, |lex| lex.slice().to_string())]
    Text(String),

    // Escaped @ which is mapped to Text in the DualLexer
    #[regex(r#"\\@"#)]
    EscapedAt,

    #[token(r#"\"#)]
    Backslash,

    #[token("@")]
    At,

    #[token("@]")]
    CloseBlock,

    #[error]
    Error,
}

pub fn split_config_string(s: &str) -> (String, String) {
    let (l, r) = s.split_once("=").unwrap();

    // @ident => ident
    (l[1..].to_string(), r.trim().to_string())
}

#[derive(Logos, Debug, PartialEq, Clone)]
pub enum CommandToken {
    // #[regex(r#"[^@]"#)]
    // Text,

    // #[regex(r"[0-9][0-9_]*", |lex| {
    //     let without_under = lex.slice().replace('_', "");

    //     u128::from_str_radix(&without_under, 10)
    // })]
    // Integer(u128),

    // #[token("@r")]
    // Repeat,

    // #[token("@[")]
    // OpenBlock,
    // #[token("@]")]
    // CloseBlock,
    #[token("h")]
    Highlight,
    #[token("s")]
    Show,

    #[token("[")]
    OpenBlock,

    #[regex(r"[0-9][0-9_]*", |lex| {
        let without_under = lex.slice().replace('_', "");
        usize::from_str_radix(&without_under, 10)
    })]
    Integer(usize),

    #[token("-")]
    Dash,

    #[regex(r#"@([A-z]+)=([^\n]+\n)"#, |lex| {
        split_config_string(lex.slice())
    })]
    ConfigLine((String, String)),

    #[error]
    Error,
}

#[derive(Clone, Debug, PartialEq)]
pub enum TokenKind {
    Eof,
    Text(String),
    At,
    CloseBlock,
    Command(CommandToken),
    Error,
}

impl TokenKind {
    pub fn text(s: &str) -> Self {
        Self::Text(s.to_string())
    }

    pub fn as_str(&self) -> &str {
        match self {
            TokenKind::Text(t) => t,
            TokenKind::At => "@",
            TokenKind::Command(c) => match c {
                CommandToken::Highlight => "h",
                CommandToken::Show => "s",
                CommandToken::OpenBlock => "[",
                CommandToken::Integer(_) => "integer",
                CommandToken::Dash => "-",
                CommandToken::Error => "error",
                CommandToken::ConfigLine(_) => "config line",
            },
            TokenKind::CloseBlock => "@]",
            TokenKind::Eof => "end of file",
            TokenKind::Error => "error",
        }
    }
}

#[derive(Clone)]
pub enum LexerKind<'a> {
    Text(Lexer<'a, TextToken>),
    Command(Lexer<'a, CommandToken>),
}

#[derive(Clone)]
pub struct DualLexer<'a> {
    pub kind: Option<LexerKind<'a>>,
}

impl<'a> DualLexer<'a> {
    pub fn command_done(&mut self) {
        match self.kind.take() {
            Some(LexerKind::Text(_)) => panic!("Exited command mode when not in command mode"),
            Some(LexerKind::Command(cmd)) => self.kind = Some(LexerKind::Text(cmd.morph())),
            None => panic!("Oh no, the lexer is gone"),
        }
    }

    pub fn enter_command_mode(&mut self) {
        match self.kind.take() {
            Some(LexerKind::Text(t)) => self.kind = Some(LexerKind::Command(t.morph())),
            Some(LexerKind::Command(cmd)) => self.kind = Some(LexerKind::Command(cmd)),
            None => panic!("Oh no, the lexer is gone"),
        }
    }

    pub fn next(&mut self) -> Option<TokenKind> {
        let lex = self.kind.take().expect("oh no, the lexer is gone");
        let (tok, lex) = match lex {
            LexerKind::Text(mut t) => match t.next() {
                Some(inner) => match inner {
                    TextToken::Text(s) => (Some(TokenKind::Text(s)), LexerKind::Text(t)),
                    TextToken::EscapedAt => {
                        (Some(TokenKind::Text("@".to_string())), LexerKind::Text(t))
                    }
                    TextToken::Backslash => {
                        (Some(TokenKind::Text("\\".to_string())), LexerKind::Text(t))
                    }
                    // When we see an @, we switch the lexer into command mode.
                    // It is up to the parser to switch it back
                    TextToken::At => (
                        Some(TokenKind::At),
                        LexerKind::Command(t.morph::<CommandToken>()),
                    ),
                    TextToken::CloseBlock => (Some(TokenKind::CloseBlock), LexerKind::Text(t)),
                    TextToken::Error => (Some(TokenKind::Error), LexerKind::Text(t)),
                },
                None => (None, LexerKind::Text(t)),
            },
            LexerKind::Command(mut c) => match c.next() {
                Some(CommandToken::Error) => (Some(TokenKind::Error), LexerKind::Command(c)),
                Some(other) => (Some(TokenKind::Command(other)), LexerKind::Command(c)),
                None => (None, LexerKind::Command(c)),
            },
        };

        self.kind.replace(lex);
        tok
    }

    pub fn span(&self) -> Range<usize> {
        self.kind
            .as_ref()
            .map(|kind| match kind {
                LexerKind::Text(l) => l.span(),
                LexerKind::Command(l) => l.span(),
            })
            .expect("Oh no, the lexer is gone")
    }
}

use std::{rc::Rc, sync::RwLock};

use codespan_reporting::term::termcolor::Buffer;
use spade_diagnostics::{emitter::CodespanEmitter, CodeBundle, DiagHandler};

use crate::{ast::Ast, parse, ErrorHandler};

pub fn parse_code(code: &str) -> Ast {
    let error_buffer = Buffer::ansi();
    let mut diag_handler = DiagHandler::new(Box::new(CodespanEmitter));

    let code_bundle = Rc::new(RwLock::new(CodeBundle::new("".to_string())));
    let mut errors = ErrorHandler {
        failed: false,
        error_buffer,
        diag_handler: &mut diag_handler,
        code: Rc::clone(&code_bundle),
    };

    let ast = parse(
        ("inline".to_string(), code.to_string()),
        code_bundle.clone(),
        true,
        &mut errors,
    );

    match ast {
        Ok(ast) => ast,
        e => {
            eprint!(
                "{}",
                String::from_utf8_lossy(errors.error_buffer.as_slice())
            );
            panic!("{e:?}")
        }
    }
}

mod ast;
mod backends;
mod lexer;
mod parser;
#[cfg(test)]
mod testutil;

use std::{io::Write, rc::Rc, sync::RwLock};

use ast::Ast;
use backends::{Backend, Latex, Typst};
use camino::Utf8PathBuf;
use clap::Parser as ClapParser;
use codespan_reporting::term::termcolor::Buffer;
use color_eyre::{
    eyre::{anyhow, Context},
    Result,
};
use logos::Logos;
use parser::Parser;
use spade_diagnostics::{emitter::CodespanEmitter, CodeBundle, CompilationError, DiagHandler};
use tracing::Level;

pub struct ErrorHandler<'a> {
    pub failed: bool,
    pub error_buffer: Buffer,
    pub diag_handler: &'a mut DiagHandler,
    /// Using a RW lock here is just a lazy way of managing the ownership of code to
    /// be able to report errors even while modifying CodeBundle
    pub code: Rc<RwLock<CodeBundle>>,
}

impl<'a> ErrorHandler<'a> {
    fn report(&mut self, err: &impl CompilationError) {
        self.failed = true;
        err.report(
            &mut self.error_buffer,
            &self.code.read().unwrap(),
            &mut self.diag_handler,
        );
        self.error_buffer
            .flush()
            .expect("failed to flush error buffer");
    }
}

trait Reportable<T> {
    /// Report the error, then discard the error, returning Some if it was Ok
    fn or_report(self, errors: &mut ErrorHandler) -> Option<T>;

    // Report the error and continue without modifying the result
    fn report(self, errors: &mut ErrorHandler) -> Self;
}

impl<T, E> Reportable<T> for Result<T, E>
where
    E: CompilationError,
{
    fn report(self, errors: &mut ErrorHandler) -> Self {
        if let Err(e) = &self {
            errors.report(e);
        }
        self
    }

    fn or_report(self, errors: &mut ErrorHandler) -> Option<T> {
        self.report(errors).ok()
    }
}

#[derive(Clone, clap::Subcommand)]
pub enum Format {
    Latex,
    Typst { prelude_file: Utf8PathBuf },
}

#[derive(ClapParser)]
struct Args {
    #[command(subcommand)]
    format: Format,
    #[arg()]
    infiles: Vec<Utf8PathBuf>,
    #[arg(short, long)]
    outdir: Utf8PathBuf,
}

fn main() -> Result<()> {
    color_eyre::install()?;

    let args = Args::parse();

    let error_buffer = Buffer::ansi();
    let mut diag_handler = DiagHandler::new(Box::new(CodespanEmitter));

    let code = Rc::new(RwLock::new(CodeBundle::new("".to_string())));
    let mut errors = ErrorHandler {
        failed: false,
        error_buffer,
        diag_handler: &mut diag_handler,
        code: Rc::clone(&code),
    };

    let (backend, extension): (Box<dyn Backend>, _) = match args.format {
        Format::Latex => (Box::new(Latex {}), "tex"),
        Format::Typst { prelude_file } => {
            let prelude = std::fs::read_to_string(&prelude_file)
                .with_context(|| format!("Failed to read typst prelude at {prelude_file}"))?;
            (Box::new(Typst { prelude }), "typ")
        }
    };

    let result = args
        .infiles
        .iter()
        .map(|f| {
            let file_content =
                std::fs::read_to_string(&f).with_context(|| format!("Failed to open {f}"))?;

            let ast = parse(
                (f.to_string(), file_content),
                code.clone(),
                false,
                &mut errors,
            )?;

            let cfgs = ast.cfgs().report(&mut errors)?;

            let target_file = args.outdir.join(
                f.with_extension(extension)
                    .file_name()
                    .ok_or_else(|| anyhow!("{f} had no filename"))?,
            );

            std::fs::write(&target_file, backend.emit(&ast, &cfgs))
                .with_context(|| format!("Failed to write output to {target_file}"))?;

            Ok(())
        })
        .collect::<Result<Vec<_>>>();

    std::io::stderr()
        .write_all(errors.error_buffer.as_slice())
        .unwrap();

    result?;

    Ok(())
}

#[tracing::instrument(skip_all)]
pub fn parse(
    (name, content): (String, String),
    code: Rc<RwLock<CodeBundle>>,
    print_parse_traceback: bool,
    errors: &mut ErrorHandler,
) -> Result<Ast> {
    // Read and parse input files
    let _span = tracing::span!(Level::TRACE, "source", ?name).entered();
    let file_id = code.write().unwrap().add_file(name, content.clone());
    let mut parser = Parser::new(lexer::TextToken::lexer(&content), file_id);

    let parse_result = parser.top_level();

    if print_parse_traceback {
        println!("Parse trace:");
        println!("{}", parser::format_parse_stack(&parser.parse_stack));
    }

    parse_result
        .map_err(|e| e)
        .or_report(errors)
        .ok_or_else(|| anyhow!("Compilation error"))
}

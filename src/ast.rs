use std::collections::BTreeMap;

use spade_common::location_info::{Loc, WithLocation};
use spade_diagnostics::Diagnostic;

pub type Result<T> = std::result::Result<T, Diagnostic>;

#[derive(Debug, PartialEq)]
pub struct Timespan {
    pub start: Option<usize>,
    pub end: Option<usize>,
}

impl Timespan {
    pub fn slide_count(&self) -> usize {
        self.start.unwrap_or(0).max(self.end.unwrap_or(0))
    }

    pub fn contains(&self, number: usize) -> bool {
        number >= self.start.unwrap_or(0) && number <= self.end.unwrap_or(usize::MAX)
    }
}

#[derive(Debug, PartialEq)]
pub enum Ast {
    Seq(Vec<Ast>),
    Text(String),
    Highlight(Timespan, Box<Ast>),
    Show(Vec<(Timespan, Ast)>),
    Cfg(Loc<(String, String)>),
}

impl Ast {
    #[cfg(test)]
    pub fn text(s: &str) -> Self {
        Self::Text(s.to_string())
    }

    #[cfg(test)]
    pub fn hl(start: Option<usize>, end: Option<usize>, inner: Vec<Ast>) -> Self {
        Self::Highlight(Timespan { start, end }, Box::new(Ast::Seq(inner)))
    }

    pub fn slide_count(&self) -> usize {
        match self {
            Ast::Cfg(_) => 0,
            Ast::Text(_) => 0,
            Ast::Seq(inner) => inner.iter().map(|i| i.slide_count()).max().unwrap_or(0),
            Ast::Show(inner) => inner
                .iter()
                .map(|(ts, sub)| ts.slide_count().max(sub.slide_count()))
                .max()
                .unwrap_or(0),
            Ast::Highlight(ts, inner) => ts.slide_count().max(inner.slide_count()),
        }
        .max(1)
    }

    pub fn cfgs(&self) -> Result<BTreeMap<String, String>> {
        fn inner(ast: &Ast, result: &mut BTreeMap<String, Loc<String>>) -> Result<()> {
            match ast {
                Ast::Seq(v) => {
                    for i in v {
                        inner(i, result)?;
                    }
                }
                Ast::Text(_) => {}
                Ast::Highlight(_, _) => {}
                Ast::Show(sub) => {
                    for (_, v) in sub {
                        inner(v, result)?;
                    }
                }
                Ast::Cfg(x) => {
                    let (k, v) = &x.inner;

                    let prev = result.insert(k.clone(), v.clone().at_loc(&x));

                    if let Some(prev) = prev {
                        return Err(Diagnostic::error(x, "Duplicate cfgs")
                            .primary_label("Duplicate cfg")
                            .secondary_label(prev, "Previous cfg here"));
                    }
                }
            }
            Ok(())
        }
        let mut result = BTreeMap::new();
        inner(self, &mut result)?;
        Ok(result.into_iter().map(|(k, v)| (k, v.inner)).collect())
    }
}

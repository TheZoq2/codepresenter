# Codepresenter

A small utility to annotate and animate code in slides. Currently there is only
a latex backend but other backends, like https://motioncanvas.io/ are possible.

## Installation

```
cargo install --git https://gitlab.com/TheZoq2/codepresenter
```

## Usage

```
codepresenter --outdir <OUTDIR> <FORMAT> [INFILES]...
```

Infiles are a list of annotated source files. Each file will result in an
output file in the specified format in OUTDIR. For latex, a file called `a.ext`
will result in `<OUTDIR>/a.tex` which can be included in a beamer presentation
using `\include{<OUTDIR>/a}`

## Syntax

In order to be unobtrusive and embeddable in most languages, all syntax is prefixed with `@`.

### Time ranges
Time ranges (which slide to show something on) are on the form
- `n` only show on slide $n$
- `n-m` show between slide $n$ and $m$ (inclusive)
- `-m` show between slide $0$ and $m$ (inclusive)
- `n-` show on all slides after and including $n$

### Highlighting
To highlight a block of code, use `@h<range>[<code>@]`
For example, to highlight the fn keyword on slide 1, 2 and 3:
```
@h1-3[fn@] main
```


### Show and Replace
To show a block of code only on specific slides, use `@s<range>[<code>@]`.

For to make the fn keyword appear on slide 2:
```
@s2[fn@] main
```

To replace a block of code, show blocks can be sequenced by `s<range>[<code1>@]@<range2>[<code>@]@<range3>[@]...`

For example, to switch between `fn`, `entity` and `pipeline(1)` on slides 1, 2 and 3:
```
@s1[fn@]@2[entity@]@3[pipeline(1)@]
```

### Metadata

Additional metadata can be specified with `@@<key>=<value>`
The rest of the line is included in the value, and the newline is discarded

Currently, two metadata fields are supported

### `@@lang`

Sets the language used for syntax highlighting purposes

```
@@lang=spade
```

### `@@lst_extra`

In latex mode, sets extra keys to give to the lstlistings being generated.

Example:
```
@@lang=spade
@@lst_extra=style=base
```
generates
```latex
\begin{lstlisting}[language=spade, style=base] ...
```

## Latex usage notes

For highlighting to work in latex, you need to define two lststyles for the language
you want to use; base and highlight. Here is an example for Spade:
```latex
\definecolor{spadeKeyword}{rgb}{0.16, 0.36, 0.60}
\definecolor{spadeType}{rgb}{0.84, 0.0, 0.37}
\definecolor{spadeConditional}{rgb}{0.46, 0.28, 0.58}
\definecolor{spadeBuiltin}{rgb}{0.84, 0.37, 0.0}
\definecolor{spadeOperator}{rgb}{0, 0, 0}
\definecolor{stringColor}{rgb}{0.37, 0.0, 1.0}
\definecolor{spadeTypeOperator}{rgb}{0, 0.37, 0.52}
\definecolor{spadeBoolLiteral}{rgb}{0.03, 0.54, 0.03}
\definecolor{spadeSelf}{rgb}{0.54, 0.03, 0.03}
\definecolor{bgColor}{rgb}{1,1,1}

\definecolor{errorRed}{rgb}{0.81,0.18,0.14}
\definecolor{errorBlue}{rgb}{0,0.24,0.55}
\definecolor{errorOrange}{rgb}{0.72,0.62,0.03}

% All of this madness is explained here:
% https://tex.stackexchange.com/questions/8384/how-to-make-overlay-still-work-inside-lstlisting-environment/18266#18266
\lstdefinestyle{highlight}{
    keywordstyle=[1]\color{spadeKeyword},
    keywordstyle=[2]\color{spadeType},
    keywordstyle=[3]\color{spadeConditional},
    keywordstyle=[4]\color{spadeBuiltin},
    keywordstyle=[5]\color{spadeOperator},
    keywordstyle=[6]\color{spadeBoolLiteral},
    keywordstyle=[7]\color{spadeTypeOperator},
    keywordstyle=[8]\color{spadeSelf},
    commentstyle=\color{commentGreen},
    stringstyle=\color{stringColor},
}
\lstdefinestyle{base}{
    language=Spade,
    basicstyle=\color{black!40}\codesize\ttfamily,
    keywordstyle=[1]\color{black!40},
    keywordstyle=[2]\color{black!40},
    keywordstyle=[3]\color{black!40},
    keywordstyle=[4]\color{black!40},
    keywordstyle=[5]\color{black!40},
    keywordstyle=[6]\color{black!40},
    keywordstyle=[7]\color{black!40},
    keywordstyle=[8]\color{black!40},
    commentstyle=\color{black!40},
    moredelim={**[is][{\color{white}\lstset{style=highlight}}]{@h}{h@}},%
    escapechar=`
}
```
Base is the "dim" style used by non-highlighted code, and `highlight` is the style
that is applied to things which should be highlighted.

You also need to specify the base style using `@@lst_extra=style=base`

## License

This project is licensed under the [EUPL-1.2 license](LICENSE-EUPL-1.2.txt).




